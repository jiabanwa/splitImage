#!python
import os,sys
from xml.etree import ElementTree
from PIL import Image
import json
 
def gen():
	jsonPath = os.path.join( os.getcwd(), "gamejson.json" )
	f = open( jsonPath, 'r' )
	fileStr = f.read()
	f.close()
	jsonData = json.loads( fileStr )
	for o in jsonData:
		name = o["images"][0]
		frames = o['frames']
		animations = o["animations"]
		
		dirName = os.path.join( os.getcwd(), str(name))
		if not os.path.isdir(dirName):
			os.mkdir(dirName)
				
		names = []
		for n in animations:
			names.append(n)
		 
		imagePath = os.path.join( os.getcwd(), str(name) +".png" )
		allImage = Image.open(imagePath) 
		  
		seq = 0
		for box in frames: 
			outfile = os.path.join( os.getcwd(),name, str(names[seq])+".png")
			x = box[0]
			y = box[1]
			w = box[2]
			h = box[3]
			box = ( x, y, x+w, y+h )
			imgData = allImage.crop( box )
			imgData.save( outfile, 'png' )
			seq = seq + 1
		
		 
		
			
	
    
if __name__ == '__main__':
    gen()
