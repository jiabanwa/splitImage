# coding: utf-8
import os
import sys
import time
import json 
from compare import compareJson,findAllFile

'''
本代码适用于laya 2.7.1 版本 文件分离模式
20201117
'''

#全局变量
cCompId = 0
PAGEDATA = {}
TOTAL_FILE = 0

'''
读取json数据
'''
def readDataJson(jsonPath):
	if not os.path.exists( jsonPath ): 
		return None
		
	f = open( jsonPath, 'r' )
	fileStr = f.read()
	jsonData = json.loads( fileStr )
	f.close() 
	return jsonData

def writeDataJson(exportName,datas):
	file = open( exportName, 'w+' )
	json_data = json.dumps( datas,indent=4 )
	#formatR = formatR.replace(" ", "");
	file.write( json_data )
	file.close()

#设置子属性模板
def getChildTemplete():
	return { 
		"x":0,
		"nodeParent":8,
		"label":"Sprite",
		"isDirectory":False,
		"isAniNode":True,
		"hasChild":False,
		"isOpen":True,
		"child":[]	
	}

def getScriptTemplete():
	return {
		"x":15,
		"type":"Script",
		"switchAble":True,
		"source":"src/game/script/Productivity.ts",
		"searchKey":"Script,Productivity",
		"removeAble":True,
		"props":{},
		"nodeParent":2,
		"label":"Productivity",
		"isDirectory":False,
		"isAniNode":True,
		"hasChild":False,
		"compId":4,
		"child":[]
	}


#自增compID
def getCompID():
	global cCompId
	cCompId = cCompId + 1
	return cCompId


#查找相同的文件
def compSubPage(pageData): 
	json1 = pageData.get(u"child") 
	items = PAGEDATA.items()
	for fileName,value in items: 
		json2 = value.get("child") 
		isRet = compareJson(json1[0],json2[0])
		if isRet == True:  
			#print fileName 
			page = {
				"x":15,
				"type":"UIView",
				"source":"xx.scene", 
				"props":{},
				"nodeParent":2,
				"isOpen":True,
				"searchKey":"UIView",
				"label":"UIView",
				"isDirectory":False,
				"isAniNode":True,
				"hasChild":False,
				"compId":5,
				"child":[]
			}
			page.update({"source":fileName.replace(".json",".scene")})
			pos = pageData.get("props")
			if pos.get("x") != None and pos.get("y")!=None:
				page.update({"props":{
					'x':pos.get("x"),'y':pos.get("y")
				}}) 
			return page

	return None

#组装子节点
def readChild(value,fileName): 
	array = [] 
	for each in value:
		type = each.get("type")
		d = getChildTemplete() 
		if type == "Script": 
			d = getScriptTemplete() 
		#//属性级别
		child3 = each.items()
		for k, v in child3:   
			if k =="type" and v !="Script":
				d.update({ "x":15 })
				props = each.get(u"props") 
				name = None
				if "var" in props:
					name = props.get("var")
				if name == None or name == v :
					d.update( { "searchKey":v})  
				else:
					d.update( { "searchKey":"{0},{1}".format(v,name)})  
				d.update({ k:v })

			elif k == "child":
				if len(v)>1:
					d.update({ "hasChild":True })
					d.update({ "isDirectory":True })
				d.update({ k:readChild(v,fileName) })
			else: 
				d.update({ k:v })

		#修正嵌套UI
		type = each.get("type")
		if type == "UIView": 
				props = each.get("props") 
				pageData = props.get(u"pageData") 
				page = compSubPage(pageData) 
				if page != None:
					#print u"发现嵌套文件:{0}".format(fileName)
					del props["pageData"]  
					d.update({ "source":page.get("source") })
					d.update({ "props":page.get("props") }) 
					d.update({ "label":type })
		if type == "Script": 
			props = each.get(u"props")
			runtime = props.get("runtime")  
			p,f = os.path.split(runtime)
			scriptName,ext = os.path.splitext(f) 
			d.update( {"source":"src/"+runtime})
			d.update( { "searchKey":"{0},{1}".format(type,scriptName)})  
			d.update( { "label":scriptName})  
			d.update( { "props":{"x":0,"y":0}})
		
			
		array.append(d)
	
	return array

def reloadJson(items,fileName):
	#场景文件还原申明
	configs = {
		"x":0,
		"type":"Scene",
		"selectedBox":2,
		"searchKey":"Scene",
		"selecteID":3,
		"nodeParent":-1,
		"maxID":14,
		"label":"Scene",
		"isOpen":True,
		"isDirectory":False,
		"isAniNode":False,
   		"hasChild":False,
		"compId":2,
		"props":{},
		"child":[],
		"animations":[],
	}
	for key, value in items:
		if key == "compId":
			configs.update( { key:value })
		if key == "type" and value == "Scene":
			configs.update( { key:value }) 
			configs.update( { "searchKey":value})
			configs.update( { "label":value }) 
		if key == "props":
			value.update({ "sceneColor":"#000000" })
			configs.update( { key:value })  
		if key == "child":
			if len(value)>1: 
				configs.update({ "isDirectory":True })
				configs.update({ "hasChild":True })
			configs.update({key:readChild(value,fileName)})
		if key == "animations":
			configs.update( { key:value })  

	return configs


def loadScene(path,outDir,fileName):
		data = readDataJson(path)
		if data == None:
			print u"读取失败"+path
		else:
			items = data.items() 
			configs = reloadJson(items,fileName)
			#保存为场景文件
			filePath = outDir+".scene"
			writeDataJson(filePath,configs) 
			global TOTAL_FILE
			TOTAL_FILE = TOTAL_FILE + 1
		

#读取指定目录	 
def find_file( path, outPath):
	for name in os.listdir( path ): 
		if os.path.isdir( os.path.join(path, name) ): 
			find_file(os.path.join(path, name),outPath )
		else:
			portion = os.path.splitext(name) 
			if portion[1] == '.json':
				#print u"开始解析：{0}".format(name)
				outDir = os.path.join(outPath,portion[0])
				tarPath = os.path.join(path,name)  
				loadScene(tarPath,outDir,name)
				#print u"完成解析："+name
			

if __name__=='__main__':

	if len( sys.argv ) < 2:
		tdir = raw_input("Enter your targerDir: ")
	else:
		tdir = sys.argv[1]
	if len( sys.argv ) < 3:
		odir = raw_input("Enter your outPath: ")
	else:
		odir = sys.argv[2]

	target = os.path.join(os.getcwd(),tdir)
	outDir = os.path.join(os.getcwd(),odir)
	#遍历所有配置
	PAGEDATA = findAllFile(target,outDir)
	if not os.path.isdir( outDir ):
		os.mkdir( outDir )
	#预读取嵌套文件 
	find_file(target,outDir)
	
	print "SUCCESS {0} file".format(TOTAL_FILE)
 
