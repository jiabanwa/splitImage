# coding: utf-8
import os
import sys
import time
import json


cCompId = 0

#保存文件映射
fileDict = {}


'''
读取json数据
'''
def readDataJson(jsonPath):
	if not os.path.exists( jsonPath ): 
		return None
		
	f = open( jsonPath, 'r' )
	fileStr = f.read()
	jsonData = json.loads( fileStr )
	f.close() 
	return jsonData

def writeDataJson(exportName,datas):
	file = open( exportName, 'w+' )
	json_data = json.dumps( datas,indent=4 )
	#formatR = formatR.replace(" ", "");
	file.write( json_data )
	file.close()
 

#读取指定目录	 
def findAllFile( path, outPath):
	for name in os.listdir( path ): 
		if os.path.isdir( os.path.join(path, name) ): 
			find_file(os.path.join(path, name),outPath )
		else:
			portion = os.path.splitext(name) 
			if portion[1] == '.json':
				#print u"开始解析：{0}".format(name)
				outDir = os.path.join(outPath,portion[0])
				tarPath = os.path.join(path,name)  
				getPageData(tarPath,name)
				#print u"完成解析："+name 

	return 	fileDict	
 
#寻找
def getPageData(path,fileName): 
	data = readDataJson(path) 
	fileDict.update({fileName:data})


#比较两个Json是否一致
def compareJson(json1,json2): 
	for src_list, dst_list in zip(sorted(json1), sorted(json2)):
		if str(json1[src_list]) != str(json2[dst_list]):
			#ret = src_list,json1[src_list],dst_list,json2[dst_list]
			return False
	
	return True
 

if __name__=='__main__':
	# 取得参数
	if len( sys.argv ) < 2:
		tdir = raw_input("Enter your targerDir: ")
	else:
		tdir = sys.argv[1]
	if len( sys.argv ) < 3:
		odir = raw_input("Enter your outPath: ")
	else:
		odir = sys.argv[2]
	target = os.path.join(os.getcwd(),tdir)
	outDir = os.path.join(os.getcwd(),odir)
	if not os.path.isdir( outDir ):
		os.mkdir( outDir )
	findAllFile(target,outDir)
	#读取文件 
	writeDataJson( os.path.join(os.getcwd(),"pageData.json"),fileDict)
	



	 
	