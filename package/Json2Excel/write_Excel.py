# -*- coding: utf-8 -*-
import os 
import sys 
import datetime
import configparser 
import xlwt
import json 
from array import array 
reload(sys)    
#type = sys.getfilesystemencoding()

def c2type(v):
	t = type(v).__name__
	if t == "int":
		return "number"
	elif t == "unicode":
		return "string"
	elif t == "list":
		return "Array<any>"
	elif t == "dict":
		return "Map<string,any>"
		
def packData(data):
	arr = []
	for i in data: 
		typ = type(i).__name__
		if typ == "int" or typ == "unicode":
			arr.append(i)
		elif typ == "list":		
			arr = i
		elif typ == "dict":
			arr = {}
			for j in i:
				arr[j]=i[j];
	return arr



def WriteConst(jsonStr):
	pass


def ReadJson(jsonStr,fileName):
	workbook = xlwt.Workbook()
	worksheet = workbook.add_sheet('export_'+fileName)
	worksheet.write(0, 0, u"id")
	worksheet.write(1, 0, "id")
	worksheet.write(2, 0, "number")
	print("export tanle name" , fileName)
	if "ConstantConf" == fileName :
		return;
	elif "VersionConf" ==	fileName:
		return
	#添加行列
	cells = None
	for o in jsonStr:
		cells = jsonStr[o]
		break
	seq = 1
	for k in cells:
		worksheet.write(0, seq, k)
		ty =t = type(cells[k]).__name__
		if ty == "list" or  ty == "dict":
			worksheet.write(1, seq, k+"_json")
		else:
			worksheet.write(1, seq, k)
		worksheet.write(2, seq, c2type(cells[k]) )
		seq +=1

	#开始写入数据 
	cellSeq = 3 
	for kes in jsonStr:
		rowSeq = 1
		rows = jsonStr[kes]
		ty = t = type(kes).__name__ 
		if ty ==  "int":
			worksheet.write(cellSeq, 0, int(kes))
		else:
			worksheet.write(cellSeq, 0, kes)
			
		for j in rows:
			o = rows[j]
			typ = type(o).__name__
			if typ == "unicode" or typ == "int" or typ == "float":
				worksheet.write(cellSeq, rowSeq, o)
			if typ == "list": 
			
				worksheet.write(cellSeq, rowSeq,json.dumps(packData(o)) )
			rowSeq +=1 
			
		cellSeq +=1
		
	#写入excel
	workbook.save(os.path.join(os.getcwd(),fileName+".xlsx"))

def readPathJson(jsonPath):
	# 开始查找
	find_file( os.getcwd(), jsonPath )
	


def find_file( path, jsonPath ):
	configs = {}
	for name in os.listdir( path ):
		if os.path.isdir( name ):
			find_file( os.path.join(path, name), jsonPath )
		else:
			portion = os.path.splitext(name)
			if portion[1] == '.json': 
				configPath = os.path.join(jsonPath,name) 
				file = open(configPath,"rb+")  # 打开文件
				with open(configPath) as f:
					data = str(f.read())
					#由于文件缺少{} 需要添加一下再解析
					configs = json.loads(  "{"+ data + "}")
					#configs = json.loads(  data )
				ReadJson(configs,os.path.splitext(name)[0])
				 
				
	 
#程序主入口
if __name__=="__main__":
	# 开始查找
	jsonPath = os.path.join( os.getcwd(), "excel_config" )
	readPathJson(jsonPath)
	
