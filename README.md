# 拆解图集功能说明

#### 项目介绍
这是目前亲测使用过图集拆解脚本和工具

#### 软件架构
依赖安装Python27,python3.x 

Image库  xml库


#### 安装教程

1. cocos  
2. coco2
3. egret
4. laya
5. PngSplit
6. package

#### 使用说明

直接使用即可 无环境直接使用exe进行拆解，有需要的可以自己扩展定制脚本，需要依赖Python环境和一些第三方库 直接pip 安装即可

各引擎下均放置了python脚本和打包exe的脚本，方便新手直接上手使用。

基本使用方式：

@call   xxx.exe  fileName  outDir

没有批量转换的需求暂时没有加目录转换。有的时候再加吧。我那么懒。



python打包exe的插件有需要的自取我放到package文件夹了

如果没有解析文件那就只有PngSplit暴力拆解或者找美术体力劳动了。

如果对您有帮助，别忘了给我点个star哦


2022/02/22 新增热更新工具插件 增加debug relese标记
 工具路径：xiutan/2.4.x热更新带分拣.zip
 增加热更新文件对比出差异文件。方便上传（每次整包上传外网简直要命）

