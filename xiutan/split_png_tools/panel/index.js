const FS = require("fire-fs");
const PATH = require('fire-path');
const Electron = require('electron');
const Exec = require('child_process').exec;
var CfgUtil = Editor.require("packages://split_png_tools/util/CfgUtil");
const creatorBatchPlist = Editor.url('packages://split_png_tools/exec/batchUnpack_plist.exe');
const layaBatchPlist = Editor.url('packages://split_png_tools/exec/laya_batchUnpack.exe');
const egretBatchPlist = Editor.url('packages://split_png_tools/exec/egret_batchUnpack.exe');
Editor.Panel.extend({
  style: `
    :host {
      padding-left: 10px;
      padding-right: 10px;

      height: auto;
    }

    .container {
      height: 100%;
      overflow-y: auto;
    }

    .button{
      float:right
    }

    ui-box-container{
      min-height: 50px;
    }
  `,

  template: `
  <div class="container">
    <h2>Plist拆分图集(window版)</h2>
    <ui-box-container class="layout vertical left">
	  <ui-prop name="图集类型">
	      <input type="radio" name="love" v-model="type" value="creator" checked><label>Creator Plist</label><br><br>
		  <input type="radio" name="love" v-model="type" value="laya"><label>Laya json atlas</label><br><br>
		  <input type="radio" name="love" v-model="type" value="egret"><label>Egret json</label><br><br>
	  </ui-prop>
	  <br>
       <ui-prop name="拆分图集目录">
            <div class="flex-1 layout horizontal center">
                <ui-input class="flex-2" disabled v-value="pngPath"></ui-input>
                <ui-button v-on:confirm="onSelectPngPath">选择</ui-button>
                <ui-button v-on:confirm="onOpenSelectPngPath">
                    <i class="icon-doc-text"></i>
                    <!--打开目录-->
                </ui-button>
            </div>
        </ui-prop>
		<br>
		<ui-prop name="输出散图目录">
            <div class="flex-1 layout horizontal center">
                <ui-input class="flex-2" v-value="outPath"></ui-input>
                <ui-button v-on:confirm="onSelectOutPath">选择</ui-button>
                <ui-button v-on:confirm="onOpenSelectOutPath">
                    <i class="icon-doc-text"></i>
                    <!--打开目录-->
                </ui-button>
            </div>
        </ui-prop>
		<br>
		<br>
		<br>
		<div class="layout horizontal flex-1 center self-end end-justified">
			<div class="self-end">
				<ui-button v-on:confirm="onBtnClickHelpDoc" @click="onStopTouchEvent">
					GitHub文档
				</ui-button>
				<ui-button v-on:confirm="onBtnClickTellMe" @click="onStopTouchEvent"
						   style="background: url('http://wpa.qq.com/pa?p=2:774177933:51');width: 77px;height: 25px;">
					 
				</ui-button>
			</div>
		</div>
		<ui-button class="button blue big" @click="save()">批量拆分</ui-button>
		<label>使用说明：<br>
			本工具不保证100%拆解成功,使用时请选择对应的打包格式<br>
			creator:大部分plist格式能正常拆解 不支持引擎自动图集<br>
			laya:引擎自带发布格式.json .atlas<br>
			creator:引擎自带图集打包.json<br>
		</label>
		<h2>日志:</h2>
		<textarea class="" id="logTextArea" v-value="logView"
				  style="width: 100%; height: 100px; background: #252525;	color: #fd942b;	border-color: #fd942b;"></textarea>
		 
    </ui-box-container>
	 
  </div>
  `,
	
	$: {
		logTextArea: '#logTextArea',
	},

  ready() {
    let logCtrl = this.$logTextArea;
	let logListScrollToBottom = function () {
		setTimeout(function () {
			logCtrl.scrollTop = logCtrl.scrollHeight; 
		}, 10);
	};
    let vue = new window.Vue({
      el: this.shadowRoot,
	  created(){
			//初始化插件   
		    this._addLog("初始化插件"); 
			this.initPlugin();
	  },
	  data:{
	    type:"creator",
		outPath:"",
		pngPath:"",
		logView:[],
	  },
      methods: {
		//面板Log
	   _addLog(str,flag) {
			if(flag){
				this.logView += "[" + flag + "]: " + str + "\n";
			}else{
				let time = new Date();
				this.logView += "[" + time.toLocaleString() + "]: " + str + "\n";
			}
			//Editor.log(this.logView);
			logListScrollToBottom();
		}, 
		  
	    //初始化插件
		initPlugin(){ 
			CfgUtil.initCfg(function (data) { 
				if (data) {
				   this.type = data.type;
				   this.outPath = data.outPath;
				   this.pngPath  = data.pngPath; 
				} else {
					this._saveConfig();
				} 
			}.bind(this));
		},
		
		//保存配置信息
		_saveConfig() { 
			CfgUtil.saveConfig(); 
		},
		
		onStopTouchEvent(event){
			event.preventDefault();
			event.stopPropagation();
		},
		
		onBtnClickHelpDoc(){
			let url = "https://gitee.com/jiabanwa/splitImage";
			Electron.shell.openExternal(url);
		},
		
		onBtnClickTellMe() {
			let url = "http://wpa.qq.com/msgrd?v=3&uin=464078717&site=qq&menu=yes";
			Electron.shell.openExternal(url);
		},
		
        save() {
		  CfgUtil.commSet("type",this.type); 
		  this._saveConfig();
		  
		  //检查效验
		  if(this.pngPath == "" || this.pngPath.length === 0){
			   this._addLog("请先设置要拆解图集的路径"); 
			   return;
		  }
		  
		  if(this.outPath == ""|| this.outPath.length === 0){
			   this._addLog("请先输出散图的路径");
			   return;
		  }
		  
		  this._addLog("当前拆分的类型是"+this.type); 
		  this._addLog("拆分图集目录"+this.pngPath); 
		  this._addLog("输出散图目录"+this.outPath);
		   let command = '';
		  switch(this.type){
			case "creator":
				command = '"' + creatorBatchPlist + '"' + ' ' + this.pngPath  + ' ' + this.outPath ; 
				break;
			case "laya":
				command = '"' + layaBatchPlist + '"' + ' ' + this.pngPath  + ' ' + this.outPath ; 
				break;
			case "egret":
				command = '"' + egretBatchPlist + '"' + ' ' + this.pngPath  + ' ' + this.outPath ; 
				break;
		  }
		  
          this._addLog('[壮壮say] 开始批量拆分'); 
		 
		  Exec(command, 
		  async (error, stdout, stderr) => {
				if (!error && stdout && !stderr) {
					Editor.log(stdout);
					Editor.log(stderr);
					this._addLog('[壮壮say] 批量拆分完成 ！');
				} else {
					if(error == null){
						this._addLog('[壮壮say] 批量拆分完成 ！')
						return
					}
					Editor.log(error); 
					if (Editor) this._addLog('[壮壮say] 拆分失败 ！');
				}
				_res();
			});
        },
		onSelectPngPath(){
			let path = Editor.Project.path;
			if (this.pngPath && this.pngPath.length > 0) {
				if (FS.existsSync(this.pngPath)) {
					path = this.pngPath;
				}
			}
			let res = Editor.Dialog.openFile({
				title: "选择要拆分图集目录",
				defaultPath: path,
				properties: ['openDirectory'],
			});

			if (res !== -1) {
				this.pngPath = res[0]; 
				CfgUtil.commSet("pngPath",this.pngPath);
				this._saveConfig(); 
			} 
			
		},
		onOpenSelectPngPath(){
			if (!FS.existsSync(this.pngPath)) {
				this._addLog("目录不存在：" + this.pngPath);
				return;
			}
			Electron.shell.showItemInFolder(this.pngPath);
			Electron.shell.beep();
			
		},
		
		onSelectOutPath(){
			let path = Editor.Project.path;
			if (this.outPath && this.outPath.length > 0) {
				if (FS.existsSync(this.outPath)) {
					path = this.outPath;
				}
			}
			let res = Editor.Dialog.openFile({
				title: "选择拆分图集输出目录",
				defaultPath: path,
				properties: ['openDirectory'],
			});

			if (res !== -1) {
				this.outPath = res[0]; 
				CfgUtil.commSet("outPath",this.outPath); 
				this._saveConfig(); 
			}
		},
		onOpenSelectOutPath(){
			if (!FS.existsSync(this.outPath)) {
				//this._addLog("目录不存在：" + this.outPath);
				this._addLog('[壮壮say]  目录不存在！');
				return;
			}
			Electron.shell.showItemInFolder(this.outPath);
			Electron.shell.beep(); 
		}
      },
    });
  },
});