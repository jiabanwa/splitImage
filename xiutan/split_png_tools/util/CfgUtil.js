var FS = require('fire-fs');
var path = require('path');
const {remote} = require('electron');
let configFileUrl = 'packages://split_png_tools/config.json';
let defaultConfig = {
    type:"creator",
	outPath:"",
	pngPath:"",
}; 

let self = module.exports = {
 
    cfgData: {
        type:"creator",
		outPath:"",
		pngPath:"",
    },

    commSet(key,value){ 
        this.cfgData[key] = value;
        this.saveConfig(); 
    }, 
    saveConfig() { 
		let filePathUrl = Editor.url(configFileUrl);
        FS.writeFile(filePathUrl, JSON.stringify(this.cfgData), function (error) {
            if (!error) { 
				//Editor.log("保存配置成功"); 
            }
        }.bind(this));
    },
	
    initCfg(cb) { 
		let filePathUrl = Editor.url(configFileUrl);
        if (FS.existsSync(filePathUrl)) { 
            FS.readFile(filePathUrl, 'utf-8', function (err, data) { 
                if (!err) {
                    let saveData = JSON.parse(data.toString());
                    self.cfgData = saveData;
                    if (cb) {
                        cb(saveData);
                    }
                }
            }.bind(self));
        } else { 
			// 写入配置
			let stringData = JSON.stringify(defaultConfig, null, '\t')
			FS.writeFileSync(filePathUrl, stringData); 
			Editor.log("配置不存在文件"); 
            if (cb) {
                cb(defaultConfig);
            }
        }
    }
};