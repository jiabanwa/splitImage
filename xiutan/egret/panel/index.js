const FS = require("fire-fs");
const PATH = require('fire-path');
const Electron = require('electron');
var http = require("http");

var CfgUtil = Editor.require("packages://egert/core/CfgUtil");
var FileDown = Editor.require("packages://egert/core/FileDown");

Editor.Panel.extend({
    style: FS.readFileSync(Editor.url('packages://egert/panel/index.css', 'utf8')) + "",
    template: FS.readFileSync(Editor.url('packages://egert/panel/index.html', 'utf8')) + "",

    $: {
        logTextArea: '#logTextArea',
    },

    ready() {
        let logCtrl = this.$logTextArea;
        let logListScrollToBottom = function () {
            setTimeout(function () {
                logCtrl.scrollTop = logCtrl.scrollHeight;
            }, 10);
        };

        window.plugin = new window.Vue({
            el: this.shadowRoot,
            created() {
                console.log("created");
                this.initPlugin();
            },
            data: {
                gameResAddr:"", //资源地址
                gameCodeAddr:"",//代码地址
                gameBaseAddr:"",//游戏根地址 
                localDownDir:"", //下载存储路径 
                isAutoDown:false, 
                gameName:"test", 
                logView: [], 
            },
            methods: {
                _addLog(str,flag) {
                    if(flag){
                        this.logView += "[" + flag + "]: " + str + "\n";
                    }else{
                        let time = new Date();
                        this.logView += "[" + time.toLocaleString() + "]: " + str + "\n";
                    } 
                    logListScrollToBottom();
                }, 
                //初始化插件
				initPlugin(){
                    CfgUtil.initCfg(function (data) {
                        if (data) { 
                           this.isAutoDown = data.isAutoDown;
                           this.localDownDir  = data.localDownDir;
                           this.gameBaseAddr = data.gameBaseAddr;
                           this.onChangeGameBaseAddr();
                        } else {
                            this._saveConfig();
                        } 
                    }.bind(this));
                },

                onChangeGameBaseAddr() { 
                    CfgUtil.setGameBaseAddr(this.gameBaseAddr);
                    this.gameResAddr = this.gameBaseAddr + "resource/default.res.json";
                    this.gameCodeAddr = this.gameBaseAddr + "manifest.json?v="+Math.random();
                }, 
 

                _isNum(str) {
                    let reg = /^[0-9]+.?[0-9]*$/;
                    if (reg.test(str)) {
                        return true;
                    } else {
                        return false;
                    }
                },

                //保存配置信息
                _saveConfig() { 
                    CfgUtil.saveConfig({
                        localDownDir:this.localDownDir, 
                    });
                    this._addLog("保存配置")
                },
                  
               
                onOpenDownDirPath(){
                    if (!FS.existsSync(this.localDownDir)) {
                        this._addLog("目录不存在：" + this.localDownDir);
                        return;
                    }
                    Electron.shell.showItemInFolder(this.localDownDir);
                    Electron.shell.beep();
                },
  
 

                 //打开本地setting文件位置
                 onSelectDownDirPath(){
                    let path = Editor.Project.path;
                    if (this.localDownDir && this.localDownDir.length > 0) {
                        if (FS.existsSync(this.localDownDir)) {
                            path = this.localDownDir;
                        }
                    }
                    let res = Editor.Dialog.openFile({
                        title: "选择本地下载存放目录",
                        defaultPath: path,
                        properties: ['openDirectory'],
                    });

                    if (res !== -1) {
                        this.localDownDir = res[0];
                        this._addLog(this.localDownDir)
                        CfgUtil.setDownDir(this.localDownDir)
                    } 
                },

                onChangeGameName(){
                    CfgUtil.commSet("isMd5",this.gameName );
                },
 

                //解析完成后自动下载资源
                clickAutoDown(){
                    this.isAutoDown = !this.isAutoDown;
                    console.log(this.isAutoDown)
                    CfgUtil.setIsAutoDown(this.isAutoDown);
                },
   

                //启动解析
                onBtnClickAnalysis(){  

                    if(!this.localDownDir || this.localDownDir == "" || this.localDownDir.length <=0){
                        this._addLog("请选择下载路径");
                        return;
                    }

                    if(!this.gameName || this.gameName == "" || this.gameName.length <=0){
                        this._addLog("建议增加一个游戏名字作为目录");
                        return;
                    }
 
                    let gameBaseDownPath = this.localDownDir+"/" + this.gameName + "/";  
                    //首页下载
                    FileDown.startDownloadTask( this.gameBaseAddr +"gameIndex.html",gameBaseDownPath, "gameIndex.html");   
                    let res = gameBaseDownPath +"/resource/"
                    FileDown.mkdirs(res,function(){}) 
                    //优先下载
                    let self  = this;
                    let fn = async function(){
                        //先下载 manifest.json 
                        await FileDown.sysncDown(self.gameResAddr,
                            res,"default.res.json").then((res )=>{
                            self._addLog("default.res.json下载完成");
                        });
                        //在下载 default.res.json
                        await FileDown.sysncDown(self.gameCodeAddr,
                            gameBaseDownPath,"manifest.json").then((res )=>{
                            self._addLog("manifest.json下载完成");
                        });   


                        let jsDir = gameBaseDownPath+"/js/"; 
                        FileDown.mkdirs(jsDir,()=>{})  

                        //读取代码文件 
                        let rawdata = FS.readFileSync(gameBaseDownPath + "/manifest.json");
                        let manifest = JSON.parse(rawdata);

                        //开始下载js代码
                        for (const key in  manifest.initial) { 
                            const url = self.gameBaseAddr  + manifest.initial[key];
                            let fileName = FileDown.getFilename(url);
                            FileDown.startDownloadTask(url,jsDir, fileName);
                        } 

                        for (const key in  manifest.game) { 
                            const url = self.gameBaseAddr  + manifest.game[key];
                            let fileName = FileDown.getFilename(url);
                            FileDown.startDownloadTask(url,jsDir, fileName);  
                        }
                        //开始下载资源
                        //读取代码文件 
                        let resData = FS.readFileSync(res+ "/default.res.json");
                        let resJson = JSON.parse(resData); 
                        let resDir = gameBaseDownPath + "/resource/";
                        let downBase = self.gameBaseAddr + "resource/"
                        FileDown.mkdirs(resDir,()=>{}); 
                        let resouces = resJson.resources
                       
                        for (const key in resouces) { 
                            const info = resouces[key];
                            //创建文件夹
                            let dirName = FileDown.getDirName(info.url)
                            let fileName = FileDown.getFilename(info.url);
                            let dirPath = resDir + dirName;
                            self._addLog(downBase + info.url)
                            FileDown.mkdirs(dirPath,function(info,dirPath,fileName){  
                                if(info.type == "sheet"){ 
                                    let url = downBase + info.url
                                    let pngUrl = url.replace(".json",".png")
                                    let name = FileDown.getFilename(pngUrl)  
                                    self._addLog("下载文件：====>"+pngUrl)
                                    FileDown.startDownloadTask( pngUrl,dirPath, name);   
                                } 
                                self._addLog("下载文件：====>"+downBase + info.url)
                                FileDown.startDownloadTask( downBase + info.url,dirPath, fileName);   

                            }.bind(this,info,dirPath,fileName))  
                        }  
                    } 
                    fn(); 
                }, 
            },
        })
    }, 
});