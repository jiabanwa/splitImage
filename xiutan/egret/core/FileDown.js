var FS = require('fire-fs');
var path = require('path');

let self = module.exports = {
        /**
         * 下载开始
         */
        startDownloadTask (imgSrc, dirName,fileName) {
            Editor.log("start downloading " + imgSrc);
            var req = http.request(imgSrc, this.getHttpReqCallback(imgSrc, dirName, fileName));
            req.setMaxListeners(50);
            req.setHeader('user-agent', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36');
            req.on('error', function(e){
                Editor.log("request " + imgSrc + " error, try again");
            });
            req.end();
        },
    /**
     * 下载回调
     */   
        getHttpReqCallback(imgSrc, dirName, fileName) {  
            var callback = function(res) {  
                if(res.statusCode == 404){
                    Editor.log("request: " + imgSrc + " return status: " + res.statusCode);
                    return
                }  
                res.setEncoding("binary");
                var contentLength = parseInt(res.headers['content-length']); 
                var downLength = 0; 
                var fileData = "";
                res.on('data', function (chunk) {
                    fileData +=chunk;
                    downLength += chunk.length;  
                });
                res.on('end', function() {
                    downFlag = false;
                    //Editor.log("end downloading " + imgSrc);
                    if (isNaN(contentLength)) {
                        Editor.log(imgSrc + " content length error");
                        return;
                    }
                    if (downLength < contentLength) {
                        Editor.log(imgSrc + " download error, try again");
                        return;
                    } 
                    FS.writeFile(dirName + "/" + fileName,fileData,"binary",function(err){
                        if(err){
                            console.log("[downloadPic]文件   "+fileName+"  下载失败.");  
                        }else{
                            Editor.log("文件"+fileName+"下载成功"); 
                        }
                    }); 

                });
            }; 
            return callback;
        },

        getFilename(filepath = () => { throw new  Error }, type = 1) { 
            let result = '';
            if (type === 1) {
                result = path.basename(filepath);
            } else if (type === 2) {
                result = path.extname(filepath);
            } else if (type === 3) {
                result = path.extname(filepath);
            } else {
                let basename = path.basename(filepath);
                let extname = path.extname(filepath);
                result = basename.substring(0, basename.indexOf(extname));
            }
            return result;
        },

        getDirName(f){ 
            return path.dirname(f)
        },

        // 递归创建目录 同步方法
        mkdirsSync(dirname) {
            if (FS.existsSync(dirname)) {
                return true;
            } else {
                if (mkdirsSync(path.dirname(dirname))) {
                    FS.mkdirSync(dirname);
                    return true;
                }
            }
        }, 
        // 递归创建目录 异步方法  
        mkdirs(dirname, callback) {  
            FS.exists(dirname, function (exists) {  
                if (exists) {  
                    callback && callback();  
                } else {   
                    self.mkdirs(path.dirname(dirname), function (dirname,callback) {  
                        FS.mkdir(dirname, callback);  
                        Editor.log('在' + path.dirname(dirname) + '目录创建好' + dirname  +'目录');
                    }.bind(this,dirname,callback));  
                }  
            });  
        },  


        //同步下载
        sysncDown(imgSrc, dirName,fileName){
            Editor.log("sysncDown");
            return new Promise(function(resovle, reject){  
                Editor.log("start downloading " + imgSrc);
                var req = http.request(imgSrc, function(imgSrc, dirName, fileName,resovle,res){
                    if(res.statusCode == 404){
                        return Editor.log("request: " + imgSrc + " return status: " + res.statusCode); 
                    }
                    res.setEncoding("binary");
                    var contentLength = parseInt(res.headers['content-length']); 
                    var downLength = 0; 
                    var fileData = "";
                    res.on('data', function (chunk) {
                        fileData +=chunk;
                        downLength += chunk.length;  
                    });
                    res.on('end', function() {
                        downFlag = false;
                        Editor.log("end downloading " + imgSrc);
                        if (isNaN(contentLength)) {
                            Editor.log(imgSrc + " content length error");
                            return;
                        }
                        if (downLength < contentLength) {
                            Editor.log(imgSrc + " download error, try again");
                            return;
                        } 
                        Editor.log("end downloading " + fileData);
 
                        FS.writeFile(dirName + "/" + fileName,fileData,"binary",function(err){
                            if(err){
                                Editor.log("[downloadPic]文件   "+fileName+"  下载失败." + err); 
                                resovle && resovle();
                            }else{
                                Editor.log("文件"+fileName+"下载成功");
                                resovle && resovle();
                            } 
                        }); 
    
                    });
                }.bind(this,imgSrc, dirName, fileName,resovle));
                req.setMaxListeners(50);
                req.setHeader('user-agent', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36');
                req.on('error', function(e){
                    Editor.log("request " + imgSrc + " error, try again");
                });
                req.end();
                });
        }
};