# coding: utf-8
import sys
reload(sys)
sys.setdefaultencoding('utf8')
import os 
import json
import time
from PIL import Image

def splitImage( fileName, outPath ):
	# 检查JSON文件
	jsonPath = "%s.json"%fileName
	
	jsonPath = os.path.join( os.getcwd(), jsonPath )
	if not os.path.exists( jsonPath ):
		print "not find %s"%jsonPath
		return
	
	# 检查PNG文件
	pngPath = "%s.png"%fileName
	pngPath = os.path.join( os.getcwd(), pngPath )
	if not os.path.exists( pngPath ):
		print "not find %s"%pngPath
		return
	
	# 检查输出目录
	outPath = os.path.join( os.getcwd(), outPath )
	if not os.path.isdir( outPath ):
		os.mkdir( outPath )
		
	# 取JSON文件
	f = open( jsonPath, 'r' )
	fileStr = f.read()
	f.close()
	jsonData = json.loads( fileStr ) 
	
	#检查image集合
	imgList = []
	imageStr = jsonData.get( "file" )
	img = Image.open(imageStr,'r')
	imgList.append(img)
	
	# 开始切图
	frames = jsonData.get( "frames" )
	for fn in frames.keys():
		data = frames.get( fn )  
		x = data.get("x")
		y = data.get("y")
		w = data.get("w")
		h = data.get("h")
		box = ( x, y, x+w, y+h )
		outFile = os.path.join( outPath, fn )
		imgData = imgList[0].crop( box )
		# if imgData.mode == "P":
		# 	imgData = imgData.convert('RGB')
		outFile = outFile.replace("_",".")
		imgData.save( outFile )
	 
	
	
if __name__=='__main__':
	# 取得参数
	if len( sys.argv ) < 2:
		fileName = raw_input("Enter your fileName: ")
	else:
		fileName = sys.argv[1]
	if len( sys.argv ) < 3:
		outPath = raw_input("Enter your outPath: ")
	else:
		outPath = sys.argv[2]
		
	# 开始切图
	splitImage( fileName, outPath )