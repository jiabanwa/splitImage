# coding: utf-8
import sys
reload(sys)
sys.setdefaultencoding('utf8')
import os 
import json
import time
from PIL import Image

def checkPath(path):
	if not os.path.exists( path ):
		print "not find 1 %s"%path
		return False
	return True


def splitImage(path, fileName, outPath ):
	# 检查JSON文件 
	jsonPath = os.path.join( path, "%s.json"%fileName  ) 
	if not os.path.exists( jsonPath ):
		print "not find %s"%jsonPath
		return
	
	# 检查PNG文件 
	pngPath = os.path.join( path,  "%s.png"%fileName ) 
	if not os.path.exists( pngPath ):
		print "not find %s"%pngPath
		return

	# 检查输出目录 
	if not os.path.isdir( outPath ):
		os.mkdir( outPath )
		

	# 取JSON文件
	f = open( jsonPath, 'r' )
	fileStr = f.read()
	f.close()
	jsonData = json.loads( fileStr ) 
	
	#检查image集合
	imgList = []
	imageStr = jsonData.get( "file" )
	img = Image.open(os.path.join(path,imageStr),'r')
	imgList.append(img)
	
	# 开始切图
	frames = jsonData.get( "frames" )
	for fn in frames.keys():
		data = frames.get( fn )  
		x = data.get("x")
		y = data.get("y")
		w = data.get("w")
		h = data.get("h")
		box = ( x, y, x+w, y+h )
		outFile = os.path.join( outPath, fn )
		imgData = imgList[0].crop( box )
		# if imgData.mode == "P":
		# 	imgData = imgData.convert('RGB')
		outFile = outFile + ".png"
		imgData.save( outFile )
	 
#读取指定目录	 
def find_file( path, outPath):
	for name in os.listdir( path ): 
		if os.path.isdir( os.path.join(path, name) ): 
			find_file(os.path.join(path, name),outPath )
		else:
			portion = os.path.splitext(name)
			if portion[1] == '.json': 
				fileName = portion[0]
				outDir = os.path.join(outPath, portion[0]);  
				splitImage( path ,fileName, outDir )
	
if __name__=='__main__':
	# 取得参数
	if len( sys.argv ) < 2:
		dirName = raw_input("Enter your dirName: ")
	else:
		dirName = sys.argv[1]
	if len( sys.argv ) < 3:
		outPath = raw_input("Enter your outPath: ")
	else:
		outPath = sys.argv[2]
	
	outPath = os.path.join( os.getcwd(), outPath )
	if not os.path.isdir( outPath ):
		os.mkdir( outPath )

	path =  os.path.join(os.getcwd(),dirName) 
	if checkPath(path): 
		# 开始切图
		find_file(path, outPath)
	
	